
#include <ESP8266WiFi.h>
#include <ThingsBoard.h>

#define WIFI_AP "hotspotku"
#define WIFI_PASSWORD "asdf1234*"

#define TOKEN "2qbXtyrClOmrWTfgNYxH"


char thingsboardServer[] = "iot.nusantarasatu.com";

WiFiClient wifiClient;

ThingsBoardSized<200> tb(wifiClient);

int status = WL_IDLE_STATUS;
unsigned long lastSend;

StaticJsonDocument<200> doc;

String uuid;
String Rssi;
String deviceName;
int count = 0;

void setup()
{
  Serial.begin(115200);
  delay(10);
  InitWiFi();
  lastSend = 0;
}

void loop()
{
  if ( !tb.connected() ) {
    reconnect();
  }

  if ( millis() - lastSend > 1000 ) { // Update and send only after 1 seconds
    lastSend = millis();
  }

  // Rx from esp32 for debug
  if (Serial.available()) {
    char rs = Serial.read();    
    count++;
    if(count <= 32){
      uuid += rs;
    }
    else if(count == 33 || count == 34){
      Rssi += rs;
    }
    else{
      deviceName += rs;
    }
    if (rs == '\n') {
      getAndSendData();
    }
  }

  tb.loop();
}

void getAndSendData()
{
  const int data_items = 5;
  Telemetry data[data_items] = {
    { "check_point", 2 },
    { "location", "South Gate" },
    { "UUID", uuid.c_str() },
    { "rssi", Rssi.toInt() },
    { "name", deviceName.c_str() }
  };
  // Debug Rx Tx
//  Serial.print(uuid.c_str());
//  Serial.print(Rssi);
//  Serial.print(deviceName);

  // Upload data to Thingsboard
    tb.sendTelemetry(data, data_items);
    count = 0;
  uuid = "";
  Rssi = "";
  deviceName = "";
}

void InitWiFi()
{
  Serial.println("Connecting to AP ...");
  // attempt to connect to WiFi network

  WiFi.begin(WIFI_AP, WIFI_PASSWORD);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("Connected to AP");
}


void reconnect() {
  // Loop until we're reconnected
  while (!tb.connected()) {
    status = WiFi.status();
    if ( status != WL_CONNECTED) {
      WiFi.begin(WIFI_AP, WIFI_PASSWORD);
      while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
      }
      Serial.println("Connected to AP");
    }
    Serial.print("Connecting to ThingsBoard node ...");
    if ( tb.connect(thingsboardServer, TOKEN) ) {
      Serial.println( "[DONE]" );
    } else {
      Serial.print( "[FAILED]" );
      Serial.println( " : retrying in 5 seconds]" );
      // Wait 5 seconds before retrying
      delay( 5000 );
    }
  }
}
