/*
   Based on Neil Kolban example for IDF: https://github.com/nkolban/esp32-snippets/blob/master/cpp_utils/tests/BLE%20Tests/SampleScan.cpp
   Ported to Arduino ESP32 by Evandro Copercini
*/

#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEScan.h>
#include <BLEAdvertisedDevice.h>

#include <WiFi.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>
#include <WiFiUdp.h>

const char* ssid = "hotspotku";
const char* password =  "asdf1234*";
const char* mqttServer = "iot.nusantarasatu.com";
const int mqttPort = 1883;
const char* mqttUser = "IM4Vs7ByyMdKpCfiZaiu";
const char* mqttPassword = "";

WiFiClient espClient;
WiFiUDP ntpUDP;
PubSubClient client(espClient);

long lastReconnectAttempt = 0;
uint8_t conn_stat = 0;                       // Connection status for WiFi and MQTT:

int scanTime = 5; //In seconds
BLEScan* pBLEScan;

int list_Counter = 0;
char buffer_data[200];

class MyAdvertisedDeviceCallbacks: public BLEAdvertisedDeviceCallbacks {  
    void onResult(BLEAdvertisedDevice advertisedDevice) {
      StaticJsonDocument<256> doc;
      // Set the values in the document
      if(advertisedDevice.haveName()){
        doc["UUID"] = advertisedDevice.getAddress().toString().c_str();
        doc["name"] = advertisedDevice.getName();      
        doc["rssi"] = advertisedDevice.getRSSI();      
        doc["location"] = "Garasi";
        doc["check_point"] = 1;            
      }else if(advertisedDevice.haveServiceUUID()){
        doc["UUID"] = advertisedDevice.getServiceUUID().toString().c_str();
        doc["name"] = advertisedDevice.getName();      
        doc["rssi"] = advertisedDevice.getRSSI();      
        doc["location"] = "Garasi";
        doc["check_point"] = 1;            
      }else{
        doc["name"] = "null";
      }                 
      
      if(doc["name"] != "null"){
        serializeJson(doc, buffer_data); 

        // POST the data to Thingsboard
        client.publish("v1/devices/me/telemetry", buffer_data);
      }                 
      
    }
};

boolean reconnect() {
  if (client.connect("ESP32Client", mqttUser, mqttPassword )) {
         Serial.println("connected");
  }
  return client.connected();
}

void setup() {
  Serial.begin(115200);
  Serial.println("Scanning...");

  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.println("Connecting to WiFi..");
  }
  conn_stat = 1;
  Serial.println("Connected to the WiFi network");

  client.setServer(mqttServer, mqttPort);  

  while (!client.connected()) {
    Serial.println("Connecting to MQTT...");

    if (client.connect("ESP32Client", mqttUser, mqttPassword )) {

      Serial.println("connected");

    } else {

      Serial.print("failed with state ");
      Serial.print(client.state());
      delay(2000);

    }
  }

  BLEDevice::init("");
  pBLEScan = BLEDevice::getScan(); //create new scan
  pBLEScan->setAdvertisedDeviceCallbacks(new MyAdvertisedDeviceCallbacks());
  pBLEScan->setActiveScan(true); //active scan uses more power, but get results faster
  pBLEScan->setInterval(100);
  pBLEScan->setWindow(99);  // less or equal setInterval value
}

void loop() {
  // put your main code here, to run repeatedly:
  BLEScanResults foundDevices = pBLEScan->start(scanTime, false);
  Serial.print("Devices found: ");
  Serial.println(foundDevices.getCount());
  Serial.println("Scan done!");
  pBLEScan->clearResults();   // delete results fromBLEScan buffer to release memory
  delay(2000);
  if ((WiFi.status() != WL_CONNECTED)) { conn_stat = 0; }
    if(conn_stat == 0){
       WiFi.begin(ssid, password);
       conn_stat = 1;
    }
    if (!client.connected()) {
    long now = millis();
    if (now - lastReconnectAttempt > 5000) {
      lastReconnectAttempt = now;
      // Attempt to reconnect
      if (reconnect()) {
        lastReconnectAttempt = 0;
      }
    }
  } else {
    // Client connected
    client.loop();
  }
  
}
